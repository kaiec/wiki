## Intro

Sometimes it is useful to pause composition of a message
and resume it later, perhaps to do some research.
Alternatively, perhaps you're running mutt itself on a remote system,
and you don't want a loss of network connectivity
to discard your new message when it cuts you off from that system
(for example, you might need to shut your laptop and run to catch a train).

In these situations programmes like [tmux](https://github.com/tmux/tmux/wiki)
and [screen](https://www.gnu.org/software/screen/)
are very useful.
They let you start a programme in its own terminal
and present you that programme in your current terminal;
you can detach or log out or lose connectivity
and the programme will continue running unaffected,
available for reattachment later.

For the sake of simplicity I will talk about tmux below.
You can do most of this with screen,
but tmux is a better application in my opinion:
easier to use, more consistent, more features.

## Composing in a tmux session

The end result of this that when you reply to a message or compose
a new message within mutt a separate mutt instance is started in a
nicely named tmux session, composing your message exactly as if the
original mutt were doing it.

However, because it is in a tmux session you can detach from the message
and resume it later.
If you don't detach you can complete your message and send it exactly
as with normal mutt.
If you do detach, you are returned to the main mutt to continue reading your email.

### Mechanism

Mutt does not have any notion of separate sessions.
However, it does have some important features:

* you can specify what editor is used to compose messages
* you can configure mutt to invoke the editor automatically
* after mutt has handed the temporary file to the editor for editing,
  if the file is unchanged when the editor returns
  then mutt silently returns to the view it was using
  before the message composition

So the process we follow to set this up is:

* provide macros for the message composition keystrokes
  (`m`: new message, `r`: reply, `g`: group reply, `L`: list reply)
  which save the default editor setting,
  set `$editor` to a script to enable the tmux mode,
  run the message action,
  restore the previous `$editor` setting.
* configure mutt to enter editor mode immediately,
  including the message headers
* provide the tmux script to use as the editor

#### Macros

Each of the following saves the current `$editor` setting as `$my_old_editor`,
sets `$editor` to `muttedit` (the script which invokes tmux),
dispatches the appropriate message mode,
restores the former `$editor` setting.

    macro index,pager m ":set my_old_editor=$editor<enter>:set editor=muttedit<enter><compose>:set editor=$my_old_editor<enter>" "compose a new message"
    macro index,pager r ":set my_old_editor=$editor<enter>:set editor=muttedit<enter><reply>:set editor=$my_old_editor<enter>" "reply to sender"
    macro index,pager g ":set my_old_editor=$editor<enter>:set editor=muttedit<enter><group-reply>:set editor=$my_old_editor<enter>" "reply to all recipients"
    macro index,pager L ":set my_old_editor=$editor<enter>:set editor=muttedit<enter><list-reply>:set editor=$my_old_editor<enter>" "reply to mailing list address"

#### Other Configuration

We need to automatically enter edit mode and include the message headers in the editor:

    set autoedit=yes
    set edit_headers=yes

If these are not your defaults you could include them in the macros above.

#### The `muttedit` script

The `muttedit` script takes the place of your normal editor
and spawns a tmux session running:

    mutt -e "set editor=$editor" -e 'unset signature' -H "$filename"

where (inside the script) `$editor` is the intended normal editor
and `$filename` is a copy of the temporary file which the original mutt created
as the new message template.

The new session has a nice title, such as:

    mutt-09dec2018-11_24-Re__save_and_select_from_mailbox_list

which contains the start date/time and the subject
in order to make identification easy later.

`muttedit` can be obtained here:

    https://bitbucket.org/cameron_simpson/css/raw/tip/bin/muttedit

It should be installed where it will be found by your `$PATH`,
for example in `$HOME/bin` or `/usr/local/bin`.

Note that the default editor invoked is `vim-flowed`, obtained here:

    https://bitbucket.org/cameron_simpson/css/raw/tip/bin/vim-flowed

which invokes [vim](https://www.vim.org) in a mode which supports
[format=flowed](http://www.mutt.org/doc/manual/#ff)
message composition.
You can override this default
with the `$MUTTEDIT_EDITOR` or `$EDITOR` environment variables.

The script also has a dependency on `rmafter`:

    https://bitbucket.org/cameron_simpson/css/raw/tip/bin/rmafter

### Composing in a split pane tmux session

`muttedit` actually has 4 modes of operation,
though all involve the basic operation of starting a separate composition mutt
in a nicely named tmux session.

* *inline*: this is the mode described above,
  where the session is presented as though the original mutt is doing the composition
  although of course you can detach from the session.
* *window*: if the environment variable `$MUTTEDIT_WINDOWPROG` is not empty,
  this is taken to be a command which starts a new terminal window
  running an arbitrary command,
  for example `xterm -e`.
  This mode dispatches a new window attached to the tmux composition session,
  returning you immediately to the original mutt.
* *detach*: this creates the tmux session but does not attach to it,
  something of a set and forget composition mode.
  You are returned immediately to the original mutt.
  This mode is only activated by the `-d` option to `muttedit`.
* *pane*: if the original mutt is already running inside a tmux session,
  `muttedit` will split the main tmux window and display
  the composition tmux session in the new pane.
  This means that you can access the original mutt
  and the composition session at the same time
  just by shifting the tmux focus.

So if you open mutt in a tmux session
you will automatically get a nice composition window below the main mutt.
Of course, that window is detachable too.

### Composing in a separate desktop window

If you set the environment variable `$MUTTEDIT_WINDOWPROG`
to a command which starts a new desktop window running an arbitrary command
then `muttedit` will us this to spawn such a window
showing the tmux composition session
and return immediately to the main mutt.

An example value for this variable might be:

    xterm -e

which will cause muttedit to invoke the command:

    xterm -e mutt -e "set editor=$editor" -e 'unset signature' -H "$filename"

Note that this new window command must accept the composition mutt command
as separate arguments in order that the command line strings are preserved.

If your preferred window programme does not accept an arbitrary command line
and instead accepts a shell command _string_
then you will need a wrapper script of some kind
in order to convert the command line arguments
into a safely quoted shell command string.

The utility scripts:

    https://bitbucket.org/cameron_simpson/css/raw/tip/bin/shqstr
    https://bitbucket.org/cameron_simpson/css/raw/tip/bin/shqstr-sh

can be used for this.
`shqstr-sh` is a slower, shell only script which can be used on systems without Python.
An example use in a script might look like this:

    #!/bin/sh
    shqstr=$( shqstr "$@" )
    exec other-programme -c "$shqstr"

which takes the command line arguments, quotes them,
then supplies them to `other-programme` which accepts a shell command string.
You would then supply this wrapper script as `$MUTTEDIT_WINDOWPROG`.
