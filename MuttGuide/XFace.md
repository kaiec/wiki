The X-Face header, devised by James Ashton, permits to embed in the mail
header the compressed representation of a 48x48x1 image, usually
representing the face of the sender.

# Create an X-Face image

At first you need to create an XBM (X Bit Map file) with dimensions
48x48X1.

You can for example use gimp, work with a pencil with a size of one
pixel on a zoomed image and use another view (View -\> New view) to see
the result of your editing in the real dimension of the image.

Or you can convert some already existing image to the XBM format, or
start from this image converted to modify it to make it more
recognizable (or to make it more like a caricature).

Then you need to compress the XBM file to the compressed format which
has to be included in the mail header: this is usually done with
libcompface and the compface utility.

compface works by filtering the XBM file and issuing on stdout the
resulting X-Face header value.

Suppose you have your tao.xbm file.

You run compface on the XBM file and you get:

    pinocchio@balocchi-server ~/.xfaces> compface tao.xbm 
     &)(Gf<KRC"C)"JoSLp(dpw"U=I!)>w:w?+ZiYku]Mz:`(ai>\UfnHs/Af'g.^_1$t?s>CL&
     o-I(^t:B]*2wmhx^a$Z.`E{>%AA/[6KS-+=?7

Alternatively starting from libavcodec 54.32.100, you can use ffmpeg to
convert from and to an X-Face image:

    ffmpeg -i tao.xbm tao.xface

This web page provide an automatic any to XBM X-face conversion:
<http://www.dairiki.org/xface/>

# Including X-face headers in outgoing emails

The obtained X-face string is still unusable in a mutt file since it
contains many mutt special chars.

Here it comes this simple script:

```perl
#!/usr/bin/perl -w
# perl script to quote my_hdr commands for Mutt.
# mainly useful for X-Face. headers.
# (c) 2004 Christoph Berg, GNU GPL.
# 2004-06-13 cb: initial version

@lines = <>;
foreach (@lines) {
    chomp;
    s/([\\;'"`\$#])/\\$1/g;
}

print join '\n', @lines;
print "\n";
```

This simply quotes the special mutt chars in the X-face and outputs the
result to stdout.

Save this script somewhere in your path (for example in $HOME/bin) and
set it executable.

At this point you can run the muttquote-x-face to
    get:

    pinocchio@balocchi-server ~/.xfaces> compface tao.xbm | muttquote-x-face 
     &)(Gf<KRC\"C)\"JoSLp(dpw\"U=I!)>w:w?+ZiYku]Mz:\`(ai>\\UfnHs/Af\'g.^_1\$t?s>CL&\n o-I(^t:B]*2wmhx^a\$Z.\`E{>%AA/[6KS-+=?7

then you can use directly this string in the mutt
file:

``` 
 my_hdr X-Face: &)(Gf<KRC\"C)\"JoSLp(dpw\"U=I!)>w:w?+ZiYku]Mz:\`(ai>\\UfnHs/Af\'g.^_1\$t?s>CL&\n o-I(^t:B]*2wmhx^a\$Z.\`E{>%AA/[6KS-+=?7
```

Still better you can cook a wrapper to all this mess in this way:

``` 
 my_hdr X-Face: `compface ~/.xfaces/tao.xbm | muttquote-x-face`
```

# Viewing X-face headers in received emails

In order to see an X-face in a mail you need an appropriate script to
filter the email text, extract the X-Face header value if present,
decompress to the XBM format and finally display it.

You can find a script which does exactly this here:
<http://www.spinnaker.de/mutt/view-x-face>

then you can bind its invocation to a convenient keybinding with a
macro, for example like
this:

`macro pager \ef "|view-x-face<enter>" "display the X-Face included in the mail"`

If your terminal and font support Unicode block characters, you can also
use the bash script found at <http://ptaff.ca/arc/x-face-unicode> in the
same way as the view-x-face script above. You'll get a 24x24 text
"image".
