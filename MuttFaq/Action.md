### What's the key (-binding) for function XYZ?

Press the "?" key while mutt is running, then you can see the current
keybindings for all existing functions in the given menu (it's menu
sensitive, see the different help pages for different menus). All
available functions are listed even if unbound (see bottom of list).

If you don't know how to press the function's key, you can use
"what-key" function to see names of keys. (1.5.3 or newer)

Or, you can use "push" command

``` 
 :push <XYZ>
```

to invoke XYZ directly.

To utilize the what-key function:

In the command prompt enter

    :exec what-key.

`Then press any key you like`

and see what mutt outputs. Press <Ctrl-G> to abort. You can then use the
obtained information to bind that specific key using the raw key value,
e.g.

``` 
 :bind index \014 command
```

Uses the key, that emits the octal value 14 to bind it to the command
command.

### How to "move", "save"?

"move" as we know it from other places is "save" in mutt. "save" as we
know it from other places is "copy" in mutt.

It may have historic ("elm", or even stupid) reasons, but it can make
sense if you accept that "normally" each message should exist only once
per user (as opposed to regular data files of which you can keep
multiple duplicates without conflict). Mutt's "save" (which is "copy" +
"mark deleted") then "moves" the single item from one folder to another
(because it's "safer"? ;-). To duplicate a message (which rarely should
be needed) mutt "copies" it, which some call "save". It's just a
different concept, but it can make sense.

### How do I save/delete/copy/print/pipe multiple messages?

The keywords to look for in the [manual.txt]([MuttWiki](home) "wikilink") and
the "?"-run-time help are

``` 
 tag
 tag-prefix
```

1.  Tag the messages you want to operate on, using **t** (tag-entry) or
    **T** (tag-pattern).
2.  Then, issue the tag-prefix command (default **;**) followed by
    whichever operation you want.

For instance, with default keybindings, **;s+archive** would save all
tagged messages to the **+archive** folder.

RTFM also "auto\_tag", however be warned: it applies only in the "index"
mode, not in the "pager", even if you have
[split-screen](/Appearance "wikilink").

For "print" & "pipe" see also "pipe\_split + pipe\_decode" &
"print\_split + print\_decode" respectively!

### How to search/ tag/ match/ view only specific msgs?

When using "search" ("/"), "tag-pattern" or "limit view" ("l") commands,
you can specify one or more patterns to make the commands apply to only
desired matches. Some are used by themselves alone ("**~p**"), other
take a regular expression as parameter ("**~f bla.\*@example**"). See
the "Patterns" section (4.2) in the manual.txt for which are available
and note [PatternQuoting](PatternQuoting) for pitfalls when combining them.

### How to mark a message as "read"?

Select the message, then hit '?' and find "clear-flag", press the
respective invocation, then select 'N' to remove the "new mail" flag.
For multiple messages at once, see previous FAQ.

For mailing lists, one can also use Ctrl-R to mark an entire
uninteresting thread as read.

If you are receiving mailing-lists the use of "procmail" to separate
them from regular mail is highly
recommended.

### How to actually delete messages marked for deletion or save read messages to "mbox"es without having to quit?

Hit "$" to purge/expunge your deleted messages.

Change to the same folder you're currently in (your "inbox") to have
mutt process "mbox" saving (mbox-hooks or simply your read messages).

### I want to use buttons with my mouse as seen on some pics!

No way, mutt is txt-only! Anything else you might have seen on some
screenshots are just GUI toys of the terminal in which mutt is run (like
eterm).

### How to see the "mailboxes" folders with new mail as with "-y" option without restarting mutt?

Enter key to "change folder", then hit '?' followed by 'TAB'. In case
you don't have default keybindings, hit '?' again instead of 'TAB' and
look out for "toggle-mailboxes". Use this for convenience:

``` 
 macro index,pager y <change-folder>?<toggle-mailboxes>
```

For ease of use, a related Q: 
When I quit from an
index view, I want to return to the mailboxes view!

It turns out this is not too difficult; you make "q" in the index menu
take you to the mailboxes menu, and then you make "q" in the browser
menu take you to a menu where you can invoke <quit>. So add these two
lines to your .muttrc:

``` 
 macro   index   q       '<change-folder>?<toggle-mailboxes>'
 macro   browser q       '<exit><quit>'
```

### How to create new folders?

Just copy or save (see FAQ above on this page) to the folder you want.
If it doesn't exist, mutt will create it for you with the [FolderFormat](FolderFormat)
you
specify.

### When paging down in a message, how to stop at the end and not jump to next one?

"pager\_stop" might help you.

### Why don't shift or control cursor keys work?

Because they are not supported by mutt! You can try whether the
"what-key" function in '?' produces a sequence for your **modified**
cursor keys that you can bind in mutt to be useful. Note the special
"bind" syntax for special keys like ESC (^\[) in the manual!
