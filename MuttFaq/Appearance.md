### How to split the screen to see index and pager/ body/ content at once?

To enable the mini-index set **$pager\_index\_lines** as you need
it.

### Can I use the "default" color of my terminal or the "transparent" color?

Yes, specify the literal 'default' as color.
Example:

`color index red default ~P`

### Why is "default" not recognized when mutt with ncurses \[5.2\] starts up?

For ncurses, it's implemented by the **use\_default\_colors()**
function. If the configure script doesn't find that, it won't compile-in
the support for 'default' into mutt. (T.E.Dickey)

### Using a "default" background does not work.

If your Mutt is linked against slang (check with "**mutt -v | grep
using**"), try to link against ncurses, which generally works better.
You can make it work with slang, too, by setting the following
environment variable: export COLORFGBG="default;default" (Gentoo users
tend to have this problem.)

### Why doesn't the background highlight color show behind white space?

This seems to be caused by an interaction between the slang library and
the "reverse" video attribute that Mutt uses by default for certain
**mono/color** objects, when Mutt is running on a terminal that supports
color. (Mutt starts out configured to the "reverse" setting for these
objects on all terminals, but will not allow the user to run a "mono"
command to explicitly configure that setting \[on any **mono/color**
object\] after startup if the session is running on a color terminal.)

Some possible ways to get the background highlight color to apply to
white space as expected:

  - build against ncurses instead of slang.
  - define TERM to be a mono-only terminal when running Mutt,
    e.g.  
    **TERM=vt100 mutt**  
    (making sure that neither TERMCAP nor
    COLORTERM are defined).  
  - use **color** commands in the Mutt initialization file to replace
    the "reverse" attribute with color attributes for each of the
    affected objects.

For example, the following would fairly closely mimic
the default value of "reverse" on a standard black-on-white xterm

``` 
   color status    white black 
   color indicator white black
   color search    white black 
   color markers   white black 
```

(or one could go ahead and use colors other than black and
white).  

  - Try running mutt within screen, even if you don't need screen's
    capabilities.

### When running in screen, transparency does not work, and there are trailing spaces filling every line when cut-and-pasting

Add the following to your ~/.screenrc and restart screen:

`defbce on`  
`term screen-bce`

(The terminal type will now be screen-bce.)

### What is the mutt colour equivalent of 'grey'?

'brightblack'. Example:

`color normal brightblack black`

### Can I have an arrow ('\>') as an indicator?

Yes. Example:

`set arrow_cursor`

### Can I highlight certain email addresses in my tree?

Yes. Please consult the "Using Colors" & "Patterns" sections of
"manual.txt". Example:

`color index cyan default '~f user@example.com'`
