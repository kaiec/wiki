### Why is a message not printed as seen in mutt?

### Why is a message not caught by spam defense when piped to?

You might suffer from bad values for print\_decode and pipe\_decode. If
you set them to "yes", then mutt sends the **rendered** message as you
see it to the external cmd. For printing this is often (but not
always!) desired, for spam defense it is never good! Make sure you
turn it to "no" before you feed your spam watch dog, and reset for
"regular" use. See also
**display\_filter**.

### How to launch a browser with text links in plain text but encoded eMails?

**urlview** comes packaged with mutt, see **contrib** dir for code and
help. Basically you pipe the message to urlview. But there are some
boobytraps to avoid, see previous faq, and also
[Attachment](MuttFaq/Attachment).

### Is there any way to search encoded messages like quoted-printable ones?

**set thorough\_search=yes** so Mutt decodes mails before searching.
