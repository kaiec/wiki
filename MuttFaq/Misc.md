### What is mutt's way to provide "templates" for sending new email?

One simple way is:

  - start a new email,
  - fill in the template data & body,
  - in the compose-menu execute the function "**write-fcc**" and
  - specify your template-folder.

Then, to use a template, apply the
"**resend-message**" function on the desired template
and continue to fill out the missing data and
body.

You can also read a template from a file with partial headers
and body text when you launch mutt with **-H file**.

### How to make random signatures?

You can specify a script that produces random STDOUT, which
then can be used as signature, if you
specify

``` 
 set signature="script|"
```

### What am I missing when I invoke mutt in batch mode from the cmd-line but it goes interactive?

Please read *carefully* "**mutt -h**" for the desired arguments (and
their *order*), and make sure you specify a "body"
file to be read from STDIN:

``` 
 mutt user@example.com < body-file
 cmd | mutt user@example.com
```

If you want an empty body (when you provide everything else via
parameters), use **/dev/null** instead of the file
name.

``` 
 mutt user@example.com < /dev/null
```

### How can I access the development versions of Mutt?

The source code is maintained in a Git repository. You can pull
your own copy for hacking with

``` 
 git clone https://gitlab.com/muttmua/mutt.git
```


Afterwards, \`git pull\` should keep you up to date.


### Where do I find the change log of the differences/ features between releases (stable, dev)?

One way is to look at the !ChangeLog file packaged with the source
distribution. However, it's cluttered with lots of
code internal noise which probably makes it hard to find
the relevant entries of interest for
end-users.

Therefore you're better off look at the UPDATING link on the [MuttWiki](home)
start page for digested
changes.

### Mutt config is pretty static, how to make it more dynamic even without [ScriptSupport](ScriptSupport)?

There are still some ways to have the config adapt
dynamically, maybe one of the [ConfigTricks](ConfigTricks) does it
for you.

### What is the meaning of "mbox", "maildir", "mailbox" and "folder"?

These terms are heavily [OverloadedNames](OverloadedNames), meaning
different people use the same terms for different
meanings.

### How to fix error 'Could not copy message'?

A general-purpose error message. Frequently, the reason is that your
"$tmpdir" **becomes** full when you try to read this
message. Either free some hdd space in that volume,
or use another volume. 

Other causes may be other 'helper' applications, including crytpo tools
used to decrypt/verify signed/encrypted mail. Enabling debug mode (if
compiled in) may reveal more helpful error output.

### Switching folders is too slow with big folders, how to speed up?

Upgrade to 1.5.15 or later (or wait for 1.6 ;), there is header caching
included. Once the cache is built, you should notice
an improvement. Beware to clear/ remove the cache
when upgrading or debugging.

### How to trigger a script on new mail?

This doesn't work before 1.5.xx, so upgrade first.
Then see the example in manual.txt about **Filters** and
**status\_format**.

### How do I help update the mutt.org site?

  - <https://bitbucket.org/mutt/www-mutt-org> contains a repository of
    the website.
  - Follow instructions such as
    <https://confluence.atlassian.com/pages/viewpage.action?pageId=271942986>
