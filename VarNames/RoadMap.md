[PageOutline](PageOutline "wikilink")

## Done, doing

* The [VarNames/List](VarNames/List) of var-names itself.  
 * keep up to date with newest releases.

* [VarNames/Script](VarNames/Script) to **automatically** translate the varnames in config files. **Done**!  
 * It's safe for configs of regular use for almost all users. Rare special cases with mutt-config generator scripts **may** fail,  
  * but those users are typically capable to fix exceptions quickly, which are few anyway (no production config failure reported yet).  
 * Try [VarNames/Script](VarNames/Script) + [VarNames/Test](VarNames/Test), report failures to be fixed.

* Timetable for Transition where **both** variants will be valid. Idea: by 2.0 new names should be **established
well
enough** to drop old pre 1.6 names safely.  
 * 1.6 will still have old ones only,  
 * 1.7 - 1.999 will have both.  
 * 2.0 will have only new ones. If it comes too early, extend to 2.2 or 2.4

* New "muttrc2" to check as config first, so you can keep both config versions around independent of version number for different binaries (pre- + post-change).  
 * version specific feature exists already: suggest to use generic muttrc for "current/ newest", version specific for old dying versions.  
 * [VarNames/Script](VarNames/Script) easily produces required new version.

## To Do

* Re-work the manual so what gets committed must be a huge update to make things more consistent in total at once.

* Rewrite the manual (not just re-organize) to reflect groups of "tasks" and such.  
 * Structure per [MuttGuide](MuttGuide) or [UseCases](UseCases)???

* **BIG** warning at end of build process to inform sys- and distro- admins, so they can inform and aid their users (see "script" above).  
 * A "post-install" script can be applied to automatically convert configs when updating,  
  * basically: `for all in /home/*; do mkdir $all/.mutt2; /Script $all/.mutt $all/mutt2; done`   
  * Check for whether file or dir: `if [ -e $all/.muttrc ]; then convert .muttrc; fi; if [ -e ~/.mutt/muttrc ]; then convert ~/.mutt; fi` 

* contact distro mutt-maintainers to discuss automatic update ideas. Add distros' mutt-contacts for completeness to [MuttPeople](MuttPeople).

* PR campaign to spread the word,  
 * i.e. mutt 2.0 could possibly make the media catch up (so that people can read about it in the news, not when updating).  
 * news sites to contact:  
  * heise.de, ...

## Ideas needed

* Documentation of synonyms:  
 * How to document old + new vars to make manual.txt useful for both while in transition?  
  * Translation table?  
  * List both entries in title of desc.?  
  * ...? (what else)  
 * Will have to be manually made (except for output of [VarNames/Script](VarNames/Script)), no automation, would be just once anyway.
