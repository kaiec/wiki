# Mutt developer resources

## Bug tracking system

For all new ticket reports please include:

 * a short meaningful summary
 * a procedure to reproduce the problem

See [DebugTips](DebugTips) for diagnostic methods to speed resolution of the bug.

The tickets from our old Trac system have been migrated into
a git repository at [trac-tickets](https://gitlab.com/muttmua/trac-tickets).
Since this is somewhat out of the way, feel perfectly free to open a new
ticket and reference one of the files in that repository.

## Documentation

Browse the [manual](https://muttmua.gitlab.io/mutt/manual-dev.html) for the
development version, updated nightly.

## Code

The source code is maintained in a [Git](https://git-scm.com/)
repository [here](https://gitlab.com/muttmua/mutt). You can pull your
own copy for hacking with

    git clone https://gitlab.com/muttmua/mutt.git

This will default to the `master` branch, where mainline development
happens. Afterwards, `git pull` should keep you up to date.

There is also the `stable` branch where the latest stable version of
mutt is tracked. Bugfixes are committed into `stable` and then merged
back into `master`, while new features go into `master`.

## Nightly snapshots

(Download a nightly snapshot of the mutt distribution
[here](https://dev.mutt.org/nightlies/)).  Note: dev.mutt.org is no
longer producing nightly snapshots.

## Release roadmap

The roadmap has the current list of bugs blocking the
release of Mutt 1.6. There is also a TODO for the next point release:
[ReleaseToDo](ReleaseToDo).

## Developer Resources

The mutt source contains some developer resources in the `contrib/`
subdirectory,
namely:

 * https://gitlab.com/muttmua/mutt/raw/master/doc/devel-notes.txt

When preparing/maintaining 3rd patches,
see:

 * https://gitlab.com/muttmua/mutt/raw/master/doc/applying-patches.txt
 * https://gitlab.com/muttmua/mutt/raw/master/doc/patch-notes.txt
 * [PatchList](PatchList)

## Brainstorming

This lists topics that we're currently collecting ideas for:

 * [NewMailHandling](NewMailHandling) overhaul
 * [UnifyMailboxInterface](UnifyMailboxInterface) (more issues than only new mail handling)
 * [MailboxDriverCleanup](MailboxDriverCleanup) and MAPI support

## Mailing lists

An overview of mailing lists (for devs, users and others) can be found
at <http://www.mutt.org/mail-lists.html>.
